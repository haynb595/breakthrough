{
    "id": "b84178dc-8eab-4a88-bc77-2d03b1e58cbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c1e770e-c80f-47a1-9121-cc7d9ec503fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84178dc-8eab-4a88-bc77-2d03b1e58cbd",
            "compositeImage": {
                "id": "e0c31fa3-4e30-4183-afc5-ff5673fa0a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c1e770e-c80f-47a1-9121-cc7d9ec503fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe0fac8-d005-44b7-a30e-ed3c863ec76e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c1e770e-c80f-47a1-9121-cc7d9ec503fb",
                    "LayerId": "8c2e96ea-0fdb-4567-ab98-065755deb2e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8c2e96ea-0fdb-4567-ab98-065755deb2e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b84178dc-8eab-4a88-bc77-2d03b1e58cbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}