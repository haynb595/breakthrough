{
    "id": "d955bbd1-7023-4fab-8226-bc7270df9848",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22dcc2c2-c22d-494b-bb52-ab385048c808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d955bbd1-7023-4fab-8226-bc7270df9848",
            "compositeImage": {
                "id": "02794e61-1701-427e-8cf0-4301fba8d40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22dcc2c2-c22d-494b-bb52-ab385048c808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b0f803-eda7-4b69-adbd-32f983ad6ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22dcc2c2-c22d-494b-bb52-ab385048c808",
                    "LayerId": "4fd1f82c-31cc-445b-acaf-e259781fa2cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "4fd1f82c-31cc-445b-acaf-e259781fa2cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d955bbd1-7023-4fab-8226-bc7270df9848",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}