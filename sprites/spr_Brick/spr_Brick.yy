{
    "id": "608ed5a3-250c-4af9-b2ae-2a60969f708c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Brick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e264190d-d4ab-4a6c-9f05-31a5e4e8d51a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608ed5a3-250c-4af9-b2ae-2a60969f708c",
            "compositeImage": {
                "id": "dbf5f1cf-8d12-4d42-a4f8-1b7a90930ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e264190d-d4ab-4a6c-9f05-31a5e4e8d51a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60bc68a-b871-4b37-8644-b5a8ae73800d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e264190d-d4ab-4a6c-9f05-31a5e4e8d51a",
                    "LayerId": "4a577174-3b1e-4150-b0e3-24214bc41a75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4a577174-3b1e-4150-b0e3-24214bc41a75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "608ed5a3-250c-4af9-b2ae-2a60969f708c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}