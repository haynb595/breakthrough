{
    "id": "02773ba8-ee45-46eb-bfab-2f02ab67214c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Ball",
    "eventList": [
        {
            "id": "bffd9fad-7c30-4bbb-bb0a-c5db20061aba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02773ba8-ee45-46eb-bfab-2f02ab67214c"
        },
        {
            "id": "58d13d9c-bb6b-4aa3-b414-83c0bdd17d49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02773ba8-ee45-46eb-bfab-2f02ab67214c"
        },
        {
            "id": "67cfe4bf-3385-469e-9338-8dc998855df5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "02773ba8-ee45-46eb-bfab-2f02ab67214c"
        },
        {
            "id": "40c92221-7c90-4892-a3e0-b4818ef1e630",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b333d4e4-2d64-46c2-9cdf-0133f41bebe5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "02773ba8-ee45-46eb-bfab-2f02ab67214c"
        },
        {
            "id": "d51c3a93-f841-42b5-8bbc-ca05ad13143f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0af4a3c1-4a4b-4b78-936d-1baebf8bb456",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "02773ba8-ee45-46eb-bfab-2f02ab67214c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d955bbd1-7023-4fab-8226-bc7270df9848",
    "visible": true
}