{
    "id": "b333d4e4-2d64-46c2-9cdf-0133f41bebe5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Brick",
    "eventList": [
        {
            "id": "b2ce764b-da6b-4644-a8bd-3e8226b6b6db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b333d4e4-2d64-46c2-9cdf-0133f41bebe5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "608ed5a3-250c-4af9-b2ae-2a60969f708c",
    "visible": true
}