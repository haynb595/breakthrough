{
    "id": "0af4a3c1-4a4b-4b78-936d-1baebf8bb456",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bat",
    "eventList": [
        {
            "id": "c9e1a6db-73ea-4b88-a822-f493675c35d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0af4a3c1-4a4b-4b78-936d-1baebf8bb456"
        },
        {
            "id": "452fbb1c-29f9-46ac-bf1c-0af589232cff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0af4a3c1-4a4b-4b78-936d-1baebf8bb456"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b84178dc-8eab-4a88-bc77-2d03b1e58cbd",
    "visible": true
}